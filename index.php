<!DOCTYPE html>
<html>
<!-- Cabecera de la página o sitio web -->
  <head>
  		<!-- Referencias de diseño, estilos y funcionalidades -->
  	<meta charset="utf-8" /> <!-- Codificación de Caracteres -->
		<!-- Responsivo sitio Web -->
    <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
    <!-- Carga de CSS -->
    <link rel="Stylesheet" href="css/style.css" /> 
	  
		

    <?php include 'function/header.php' ?>
    <script src="/js/index.js"></script>
    <title> Marista - Formulario de Registro</title>

  </head>

<!-- Cuerpo de página --> 
  <body>
       
    <section>
        
        <figure >
          <img src="img/user.png" alt="foto"/>
          <figcaption>Pepe Potamo</figcaption>
        </figure>
       
        <div class="texto">
                <h2>Biografía</h2>
                <p>Joven profesional del área informática con deseos de aprender y superar sus conocimientos día a día. Amplio conocimiento en la gestión y desarrollo de proyecto de media o gran envergadura, con resultados satisfactorios que le respaldan.</p>
                <p>Joven profesional del área informática con deseos de aprender y superar sus conocimientos día a día. Amplio conocimiento en la gestión y desarrollo de proyecto de media o gran envergadura, con resultados satisfactorios que le respaldan.</p>
                <p>Joven profesional del área informática con deseos de aprender y superar sus conocimientos día a día. Amplio conocimiento en la gestión y desarrollo de proyecto de media o gran envergadura, con resultados satisfactorios que le respaldan.</p>
         </div> 
 
    </section>
  	<!-- Formulario Registro  - Llamada con PHP -->  	
    <?php include 'function/formulario.php'; ?>

  </body>

<!-- Pie de página -->

  <footer>
    <!-- Footer del Sitio con marca registrada  - Llamada con PHP -->
    <?php include 'function/footer.php' ?>
  </footer>
  

</html>

