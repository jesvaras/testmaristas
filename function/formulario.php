
<!-- Código del Formulario -->
    <form id="formulario_registro" method="post"> <!-- Se utiliza metodo post para el envío de información -->
       <!-- Formulario creado con diseño -->
        <div class="tabla_div"> 
            <h3>Datos de Registro</h3>
          <table   border="0">
              <body>
                <tr>
                  <td><label for="nombre">Nombre</label></td>
                  <td><input type="text" name="nombre" required> </td>
                </tr>
                <tr>
                  <td><label for="apellidos">Apellidos</label></td>
                  <td><input type="text" name="apellidos" required> </td>
                </tr>
                <tr>
                  <td><label for="rut">RUT</label></td>
                  <td><input type="text" name="rut" placeholder="Ej. 12345678-K" required> </td>
                </tr>
                <tr>
                  <td><label for="telefono">Teléfono</label></td>
                  <td><input type="text" name="telefono" required> </td>
                </tr>
                  <tr>
                  <td><label for="telefono">Email</label></td>
                  <td><input type="email" name="email" required> </td>
                </tr>
                 <tr>
                  <td><label for="dir">Dirección</label></td>
                  <td><input type="text" name="dir" required> </td>
                </tr>
                  
               <tr><!-- Botones con diseño de Boostrap median invocación de clases nativas  -->
                  <td><button id="Guardar" type="submit" class="btn btn-success">Guardar</button></td>
                  <td><button id="Limpiar" type="reset" class="btn btn-primary" value="reset">Limpiar</button></td>
                </tr>
              </body>
            </table> 
        </div>  
        </form> 

<!-- JavaScript -->

<script>
/*Código JS para validación */
    $(document).ready(function() {
        /* Señalo que la validación se aplica para el "formulario_registro" */
      $("#formulario_registro").validate();
      
      %("#formulario_registro").submit(function( event ) {
        alert( "Handler for .submit() called." );
        event.preventDefault();
        });

      /* Función para realizar limpieza de los campos vía JS */
      $("#Limpiar").unbind();
      $("#Limpiar").click(function()
      {
          $('#formulario_registro')[0].reset();  
      })
          
    });
    
</script>